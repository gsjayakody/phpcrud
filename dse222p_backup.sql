-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2024 at 03:19 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dse222p`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MobileNumber` varchar(11) NOT NULL,
  `Birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`Id`, `FirstName`, `LastName`, `MobileNumber`, `Birthday`) VALUES
(2, 'After Edit', 'LastName 2', '1234566789', '2024-03-10'),
(3, 'FirstName 2', 'LastName 2', '3256732', '2024-04-02'),
(5, 'John', 'Doe', '0712345678', '2000-01-01'),
(6, 'John', 'Doe', '0712345678', '2000-01-01'),
(7, 'John1', 'Doe1', '0712345678', '2000-01-01'),
(8, 'Test', 'Test', '1223243343', '2024-04-12'),
(9, 'Test', 'Test', '234343', '2024-04-03'),
(10, 'Test First Name', 'Test Lname', '1223243343', '2024-05-15'),
(11, '', '', '', '0000-00-00'),
(12, 'tet', 'ere', '234', '2024-05-08'),
(13, '', '', '', '0000-00-00'),
(14, '', '', '', '0000-00-00'),
(15, '', '', '', '0000-00-00'),
(16, 'trtq', 'ere', '3434', '2024-05-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
